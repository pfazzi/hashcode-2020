<?php
declare(strict_types=1);

namespace PedPlusPlus\HashCode2020;

class Allocation
{
    public Library $library;
    /** @var Book[] */
    public array $books;

    public function __construct(Library $library, $books)
    {
        $this->library = $library;
        $this->books   = $books;
    }
}
