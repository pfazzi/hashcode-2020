<?php

namespace PedPlusPlus\HashCode2020;

class Main
{
    public static function main()
    {
        $files = scandir(__DIR__.'/../input/');

        foreach ($files as $file) {
            if (in_array($file, ['.', '..'])) {
                continue;
            }
            self::runFile(__DIR__.'/../input/' . $file);
        }
    }

    public static function runFile($filename): int
    {
        $lines = file_get_contents($filename);
        $lines = explode(PHP_EOL, $lines);
        $lines = array_filter($lines, fn ($str) => !empty(trim($str)));
        $lines = array_map(fn ($line) => array_map('intval', explode(' ', $line)), $lines);

        $env = self::parseEnv($lines[0], $lines[1]);

        $parsedLibraries = [];
        while (count($parsedLibraries) < $env->numberOfLibraries) {
            $id = count($parsedLibraries);
            $index = 2 + ($id * 2);
            $parsedLibraries[] = self::parseLibraryData($env, $id, $lines[$index],$lines[$index + 1]);
        }

        $outputfile = str_replace('input', 'output', $filename);
        $outputfile = str_replace('.txt', '.' . time() . '.txt', $outputfile);
        self::run($env, $parsedLibraries, $outputfile);

        return 0;
    }

    private static function parseLibraryData(Env $env, int $id, array $line1, array $line2): Library
    {
        [$numberOfBooks, $signUpProcessLen, $booksPerDay] = $line1;

        $booksId = $line2;

        return new Library(
            $id,
            $numberOfBooks,
            $signUpProcessLen,
            $booksPerDay,
            array_map(fn($bookId) => $env->getBook($bookId),$booksId),
        );

    }

    private static function parseEnv(array $line1, array $line2): Env
    {
        [$books, $libraries, $days] = $line1;
        $scores = $line2;

        return new Env(
            $books,
            $scores,
            $libraries,
            $days
        );
    }

    /**
     * @param Env $env
     * @param Library[] $libraries
     * @param string $outputfile
     */
    private static function run(Env $env, array $libraries, string $outputfile)
    {
        $libraries = array_filter(
            $libraries,
            fn (Library $library): bool => $library->signUpProcessLen <= $env->days,
        );

        foreach ($libraries as $library) {
            $scores[] = $library->weights($env);
        }

        $max = array_fill(0, count($scores[0]), 0);
        foreach ($scores as $score) {
           foreach ($score as $i => $weight) {
               if ($max[$i] < $weight[0]) {
                   $max[$i] = $weight[0];
               }
           }
        }

        usort(
            $libraries,
            function (Library $left, Library $right) use ($env, $max): int {
                return $left->score($env, $max) <=> $right->score($env, $max);
            }
        );

        echo "Running algorithm...\n";
        $allocations = self::runAlgorithm($env, $libraries);

        $outputAllocations = [];
        foreach ($allocations as $libraryId => $allocation) {
            $outputAllocations[] = new Allocation($allocation['library'], $allocation['books']);
        }

        $solution = OutputFormatter::format(new Output($outputAllocations));

        echo "Writing $outputfile\n";
        file_put_contents($outputfile, $solution);
    }

    private static function head(array $array)
    {
        if (empty($array)) {
            throw new \InvalidArgumentException();
        }
        return array_slice(array_values($array), 0, 1)[0];
    }

    private static function tail(array $array)
    {
        return array_slice(array_values($array), 1);
    }

    /**
     * @param Env $env
     * @param array $libraries
     *
     * @return array
     */
    public static function runAlgorithm(Env $env, array $libraries): array
    {
        $alreadyScannedBooks = [];

        $inactiveLibraries   = $libraries;
        $allocations         = [];
        $signingUpLibrary    = null;
        $remainingSignUpDays = 0;
        $signedUpLibraries   = [];
        for ($day = 0; $day < $env->days; $day++) {
            if (null === $signingUpLibrary || $remainingSignUpDays === 0) {
                if (null !== $signingUpLibrary) {
                    $signedUpLibraries[] = $signingUpLibrary;
                }
                if (!empty($inactiveLibraries)) {
                    $signingUpLibrary    = self::head($inactiveLibraries);
                    $inactiveLibraries   = self::tail($inactiveLibraries);
                    $remainingSignUpDays = $signingUpLibrary->signUpProcessLen;
                }
            } else {
                $remainingSignUpDays--;
            }

            foreach ($signedUpLibraries as $signedUpLibrary) {
                for ($index = 0; $index < $signedUpLibrary->booksPerDay; $index++) {
                    while ($nextBook = $signedUpLibrary->getNextBook()) {
                        if (in_array($nextBook->id, $alreadyScannedBooks)) {
                            continue;
                        }

                        break;
                    }

                    if (false === $nextBook) {
                        continue 2;
                    }

                    $alreadyScannedBooks[] = $nextBook->id;
                    $allocations[$signedUpLibrary->id]['books'][] = $nextBook;
                    $allocations[$signedUpLibrary->id]['library'] ??= $signedUpLibrary;
                }
            }
        }

        return $allocations;
}
}
