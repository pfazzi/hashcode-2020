<?php
declare(strict_types=1);

namespace PedPlusPlus\HashCode2020;

class Output
{
    /** @var Allocation[] */
    public array $allocations;

    public function __construct($allocations)
    {
        $this->allocations = $allocations;
    }
}
