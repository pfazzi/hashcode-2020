<?php
declare(strict_types=1);

namespace PedPlusPlus\HashCode2020;

class Env
{
    public int $totalNumberOfBooks;
    /** @var Book[] */
    public array $books;
    public int $numberOfLibraries;
    public int $days;

    public function __construct(int $totalNumberOfBooks, array $scores, int $numberOfLibraries, int $days)
    {
        $this->totalNumberOfBooks = $totalNumberOfBooks;
        $this->numberOfLibraries  = $numberOfLibraries;
        $this->days              = $days;

        foreach ($scores as $id => $score) {
            $this->books[$id] = new Book($id, $score);
        }
    }

    public function getBook($bookId): Book
    {
        return $this->books[$bookId];
    }

    public function booksScoreSum(): int
    {
        $sum = 0;

        foreach ($this->books as $book) {
            $sum += $book->score;
        }

        return $sum;
    }
}
