<?php
declare(strict_types=1);

namespace PedPlusPlus\HashCode2020;

class Library
{
    public int $id;
    public int $numberOfBooks;
    public int $signUpProcessLen;
    public int $booksPerDay;
    /** @var Book[] */
    public array $books;

    public function __construct(int $id, $numberOfBooks, $signUpProcessLen, $booksPerDay, $books)
    {
        $this->numberOfBooks    = $numberOfBooks;
        $this->signUpProcessLen = $signUpProcessLen;
        $this->booksPerDay      = $booksPerDay;
        $this->books          = $books;
        $this->id = $id;

        usort(
            $this->books,
            fn (Book $a, Book $b) => ($a->score <=> $b->score) * (-1)
        );
    }

    public function weights(Env $env): array
    {
        $avgBookScore = $this->booksScoreSum() / $this->numberOfBooks;
        $avgScorePerDay = $avgBookScore * $this->booksPerDay;
        $totalRequiredTime = $this->numberOfBooks / $this->booksPerDay;
        $waste = 1 / ($avgScorePerDay * $this->signUpProcessLen);
        $totalScore = $this->booksScoreSum();

        return [
            [$totalRequiredTime, 1],
//            [$totalScore, 1],
//            [$avgScorePerDay, 1],
//            [$waste, 1],
        ];
    }

    public function score(Env $env, array $max)
    {
        $terms = $this->weights($env);

        $score = 0;
        foreach ($terms as $index => $term) {
            $score += ($term[0] / $max[$index]) * $term[1];
        }

        return $score;
    }

    private function booksScoreSum(): int
    {
        $sum = 0;

        foreach ($this->books as $book) {
            $sum += $book->score;
        }

        return $sum;
    }

    public function getNextBook()
    {
        return next($this->books);
    }
}
