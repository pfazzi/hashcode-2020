<?php
declare(strict_types=1);

namespace PedPlusPlus\HashCode2020;

class Book
{
    public int $id;
    public int $score;

    public function __construct(int $id, int $score)
    {
        $this->id    = $id;
        $this->score = $score;
    }
}
