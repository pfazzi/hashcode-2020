<?php
declare(strict_types=1);

namespace PedPlusPlus\HashCode2020;

class OutputFormatter
{
    public static function format(Output $output): string
    {
        $lines = [];

        $lines[] = [count($output->allocations)];

        foreach ($output->allocations as $allocation) {
            $lines[] = [
                $allocation->library->id,
                count($allocation->books)
            ];

            $lines[] = array_map(fn (Book $book) => $book->id, $allocation->books);
        }

        $lines = array_map(
            fn (array $values) => implode(' ', $values),
            $lines
        );

        return implode(PHP_EOL, $lines);
    }
}
